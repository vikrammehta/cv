import Dashboard from './components/Dashboard/Dashboard';
import { Provider } from './context/Context'

function App() {
    return (
        <Provider>
            <div className="App">
                <Dashboard />
            </div>
        </Provider>
    );
}

export default App;
