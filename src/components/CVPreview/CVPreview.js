import React from 'react'
import Education from '../Education/Education'
import PersonalDetails from '../PersonalDetails/PersonalDetails'
import ProfessionalSummary from '../ProfessionalSummary/ProfessionalSummary'
import WorkExperience from '../WorkExperience/WorkExperience'

const CVPreview = () => {
    return (
        <div className='documentHolderBg'>
            <div className='documentHolder'>
                <div className='cvDocumentPage'>
                    <div className='cvDocumentLayout'>
                        <PersonalDetails />
                        <ProfessionalSummary />
                        <WorkExperience />
                        <Education />
                    </div>
                </div >
            </div>
        </div>
    )
}

export default CVPreview
