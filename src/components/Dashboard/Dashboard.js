import React, { useContext } from 'react'
import FormWrapper from '../FormWrapper/FormWrapper'
import CVPreview from '../CVPreview/CVPreview'
import { Context } from '../../context/Context'

const Dashboard = () => {
    const { formInfo, handleOnChange, formData, handleIncrementStep, handleDecrementStep } = useContext(Context)
    const { schema, uiSchema } = formInfo

    return (
        <div className='pageWrapper'>
            <FormWrapper schema={schema} uiSchema={uiSchema} formData={formData} handleOnChange={handleOnChange} handleIncrementStep={handleIncrementStep} handleDecrementStep={handleDecrementStep} />
            <CVPreview />
        </div>
    )
}

export default Dashboard
