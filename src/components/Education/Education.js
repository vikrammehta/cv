import React, { useContext } from 'react'
import isEmpty from 'lodash/isEmpty';
import { Context } from '../../context/Context'

const Education = () => {
    const { formData } = useContext(Context)
    const { ed_title: title, ed_educationDetails: educationDetails } = formData
    const firstItem = educationDetails[0]
    const { institution, area, studyType } = firstItem

    if (isEmpty(educationDetails)) return null

    return (title || institution || area || studyType) && (
        <div className='educationDetails block'>
            {title && <h3 className='subtitle'>{title}</h3>}
            {educationDetails.map((item) => {
                const { institution, area, studyType, from, to, summary } = item
                return (
                    <div className='singleEducationDetails' key={institution}>
                        <div className='singleEducationMain'>
                            {studyType && institution && <h4 className='studyTypeTitle'>{studyType}, {institution}</h4>}
                            {from && to && <p className='fromToTitle'>{from} - {to}</p>}
                            {summary && <p>{summary}</p>}
                        </div>
                    </div>
                )
            })}
        </div>
    )
}

export default Education
