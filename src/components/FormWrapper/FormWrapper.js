import React from 'react'
import Form from '@rjsf/core'

const FormWrapper = (props) => {
    const { schema, uiSchema, formData, handleOnChange, handleIncrementStep, handleDecrementStep } = props

    return (
        <div className='formHolder'>
            <Form schema={schema} uiSchema={uiSchema} formData={formData} onChange={handleOnChange} onSubmit={() => console.log('Form Submitted')} >
                <div>
                    <button type="submit" onClick={handleIncrementStep}>Next Step</button>
                    <button type="button" onClick={handleDecrementStep}>Go Back</button>
                </div>
            </Form>
        </div>
    )
}

export default FormWrapper
