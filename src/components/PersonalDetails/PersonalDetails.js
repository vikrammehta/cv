import React, { useContext } from 'react'
import isEmpty from 'lodash/isEmpty';
import { MdLocalPhone, MdEmail, MdLocationOn } from 'react-icons/md'
import { FaGlobeAmericas } from 'react-icons/fa'
import Profiles from './Profiles/Profiles';
import { Context } from '../../context/Context'

const PersonalDetails = () => {
    const { formData } = useContext(Context)
    const { pd_name: name, pd_label: label, pd_address: address, pd_email: email, pd_phone: phone, pd_website: website, pd_profiles: profiles } = formData

    return (name || label || address || email || phone || website) && (
        <div className='personalDetails block'>
            <h1>{name}</h1>
            {label && <h2>{label}</h2>}
            {address && (
                <p>
                    <span>
                        <MdLocationOn />{address}
                    </span>
                </p>
            )}
            {(email || phone || website) && (
                <p>
                    {phone && (
                        <a href={`tel:+01-${phone}`} rel="nofollow" className='spacer'>
                            <MdLocalPhone />{phone}
                        </a>
                    )}
                    {email && (
                        <a href={`mailto:${email}`} rel="nofollow" className='spacer'>
                            <MdEmail />{email}
                        </a>
                    )}
                    {website && (
                        <a href={`https://${website}`} rel="nofollow noreferrer" target="_blank">
                            <FaGlobeAmericas />{website}
                        </a>
                    )}
                </p>
            )}
            {!isEmpty(profiles) && <Profiles profiles={profiles} />}
        </div>
    )
}

export default PersonalDetails
