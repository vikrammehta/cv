import React from 'react'
import { FaGlobeAmericas, FaGithub, FaTwitter, FaYoutube, FaBitbucket } from 'react-icons/fa'
import { AiFillLinkedin } from 'react-icons/ai'

const ProfileIcon = ({ network }) => {
    if (!network) return null
    let lcNetwork = network.toLowerCase()
    switch (lcNetwork) {
        case 'github':
            return <FaGithub />
        case 'linkedin':
            return <AiFillLinkedin />
        case 'twitter':
            return <FaTwitter />
        case 'youtube':
            return <FaYoutube />
        case 'bitbucket':
            return <FaBitbucket />
        default:
            return <FaGlobeAmericas />
    }
}

export default ProfileIcon
