import React from 'react'
import ProfileIcon from '../ProfileIcon/ProfileIcon'

const Profiles = (props) => {
    const { profiles } = props
    return (
        <p>
            {profiles.map((item) => {
                const { network, url, displayUrl } = item
                if (!network || !url) return null
                return (
                    <a href={url} rel="nofollow noreferrer" target="_blank" className='spacer' key={`${network}${url}`}>
                        <ProfileIcon network={network} />
                        {displayUrl ? url : network}
                    </a>
                )
            })}
        </p>
    )
}

export default Profiles
