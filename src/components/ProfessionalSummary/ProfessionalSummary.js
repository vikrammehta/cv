import React, { useContext } from 'react'
import isEmpty from 'lodash/isEmpty';
import { Context } from '../../context/Context'

const getTag = (string) => {
    switch (string) {
        case "Paragraph":
            return 'p'
        case "List":
            return 'li'
        default:
            return 'p'
    }
}

const getParentTag = (string) => {
    switch (string) {
        case "Paragraph":
            return 'div'
        case "List":
            return 'ul'
        default:
            return 'div'
    }
}

const ProfessionalSummary = () => {
    const { formData } = useContext(Context)
    console.log(formData)
    const {
        ps_title: title,
        ps_professionalSummary: professionalSummary,
        ps_layoutType: layoutType } = formData
    const firstItem = professionalSummary[0]
    const { detail } = firstItem
    const Tag = getTag(layoutType)
    const ParentTag = getParentTag(layoutType)

    if (isEmpty(professionalSummary)) return null

    return (title || detail) && (
        <div className='professionalSummary block'>
            {title && <h3 className='subtitle'>{title}</h3>}
            <ParentTag className={ParentTag === 'ul' ? 'summaryList' : ''}>
                {professionalSummary.map((item, index) => {
                    const { detail } = item
                    return detail && (
                        <Tag
                            className='singleProfessionalSummary'
                            key={`${detail}${index}`}>
                            {detail}
                        </Tag>
                    )
                })}
            </ParentTag>
        </div>
    )
}

export default ProfessionalSummary
