import React, { useContext } from 'react'
import isEmpty from 'lodash/isEmpty';
import { Context } from '../../context/Context'

const WorkExperience = () => {
    const { formData } = useContext(Context)
    const { wk_title: title, wk_workExperience: workExperience } = formData
    const firstItem = workExperience[0]
    const { company, position } = firstItem

    if (isEmpty(workExperience)) return null

    return (title || company || position) && (
        <div className='workExperience block'>
            {title && <h3 className='subtitle'>{title}</h3>}
            {workExperience.map((item) => {
                const { company, position, from, to, summary, highlights } = item
                return (
                    <div className='singleWorkExperience' key={company}>
                        <div className='singleWorkMain'>
                            {position && company && <h4 className='positionTitle'>{position}, {company}</h4>}
                            {from && to && <p className='fromToTitle'>{from} - {to}</p>}
                            {summary && <p>{summary}</p>}
                        </div>
                        {!isEmpty(highlights) && (
                            <ul className='singleWorkHighlights'>
                                {highlights.map((item) => {
                                    return item && <li>{item}</li>
                                })}
                            </ul>
                        )}
                    </div>
                )
            })}
        </div>
    )
}

export default WorkExperience
