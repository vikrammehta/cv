import React, { useState, useEffect } from 'react'
import { personalDetailsSchema, personalDetailsUiSchema, personalDetailsData, workExperienceSchema, workExperienceUiSchema, workExperienceData, educationSchema, educationUiSchema, educationData, professionalSummaryData, professionalSummarySchema, professionalSummaryUiSchema } from '../schema/schema'

const formProps = [
    {
        schema: personalDetailsSchema,
        uiSchema: personalDetailsUiSchema
    },
    {
        schema: professionalSummarySchema,
        uiSchema: professionalSummaryUiSchema
    },
    {
        schema: workExperienceSchema,
        uiSchema: workExperienceUiSchema
    },
    {
        schema: educationSchema,
        uiSchema: educationUiSchema
    }
]

const totalSteps = formProps.length

export const Context = React.createContext()

export const Provider = (props) => {
    const [formStep, setFormStep] = useState(1)
    const [formData, setFormData] = useState({
        ...personalDetailsData,
        ...professionalSummaryData,
        ...workExperienceData,
        ...educationData
    })
    const [formInfo, setFormInfo] = useState(formProps[formStep - 1])

    const handleIncrementStep = () => {
        if (formStep >= totalSteps) {
            setFormStep(totalSteps)
        } else {
            setFormStep(formStep + 1)
        }
    }

    const handleDecrementStep = () => {
        if (formStep <= 1) {
            setFormStep(1)
        } else {
            setFormStep(formStep - 1)
        }
    }

    const handleOnChange = (e) => {
        console.log(e.formData)
        setFormData(e.formData)
    }

    useEffect(() => {
        setFormInfo(formProps[formStep - 1])
    }, [formStep])

    // useEffect(() => {
    //     console.log(formData)
    // }, [formData])

    const globalState = {
        formProps,
        totalSteps,
        formStep,
        formData,
        formInfo,
        handleOnChange,
        handleIncrementStep,
        handleDecrementStep
    }

    return (
        <Context.Provider value={globalState}>
            {props.children}
        </Context.Provider>
    )
}