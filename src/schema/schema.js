export const professionalSummarySchema = {
    title: "Professional Summary",
    type: "object",
    properties: {
        ps_title: {
            type: "string",
            title: "Professional Summary Title"
        },
        ps_professionalSummary: {
            type: "array",
            title: "Single Professional Summary",
            minItems: 1,
            items: {
                type: "object",
                properties: {
                    detail: {
                        type: "string"
                    }
                }
            }
        },
        ps_layoutType: {
            type: "string",
            title: "Select Layout Type",
            enum: ["List", "Paragraph"]
        }
    }
}

export const professionalSummaryUiSchema = {
    ps_title: {
        "ui:placeholder": "Title for Professional Summary / Objective",
        "ui:options": {
            label: false
        }
    },
    ps_professionalSummary: {
        items: {
            detail: {
                "ui:placeholder": "A short professional summary / objective",
                "ui:widget": "textarea",
                "ui:options": {
                    label: false
                }
            }
        }
    },
    ps_layoutType: {
        "ui:options": {
            label: true
        }
    }
}

export const professionalSummaryData = {
    ps_title: "",
    ps_layoutType: "Paragraph",
    ps_professionalSummary: [
        {
            detail: ""
        }
    ]
}

export const personalDetailsSchema = {
    title: "Personal Details",
    type: "object",
    properties: {
        pd_name: {
            type: "string"
        },
        pd_label: {
            type: "string"
        },
        pd_email: {
            type: "string",
            format: "email"
        },
        pd_phone: {
            type: "string"
        },
        pd_website: {
            type: "string"
        },
        pd_address: {
            type: "string"
        },
        pd_profiles: {
            type: "array",
            title: "Social Media",
            items: {
                "$ref": "#/definitions/profile"
            }
        }
    },
    definitions: {
        profile: {
            type: "object",
            properties: {
                network: {
                    type: "string"
                },
                url: {
                    type: "string"
                },
                displayUrl: {
                    type: "boolean"
                }
            }
        }
    }
}

export const personalDetailsUiSchema = {
    classNames: "",
    pd_name: {
        classNames: "input-name",
        "ui:placeholder": "Full Name",
        "ui:options": {
            label: false
        }
    },
    pd_label: {
        classNames: "input-label",
        "ui:placeholder": "Job label / Position",
        "ui:options": {
            label: false
        }
    },
    pd_email: {
        classNames: "input-email",
        "ui:placeholder": "Email",
        "ui:options": {
            label: false
        }
    },
    pd_phone: {
        classNames: "input-phone",
        "ui:placeholder": "Phone",
        "ui:options": {
            inputType: "tel",
            label: false
        }
    },
    pd_website: {
        classNames: "input-website",
        "ui:placeholder": "Website (without https://)",
        "ui:options": {
            label: false
        }
    },
    pd_address: {
        classNames: "input-address",
        "ui:placeholder": "Your Current Locaction (Address)",
        "ui:widget": "textarea",
        "ui:options": {
            label: false
        }
    },
    pd_profiles: {
        items: {
            network: {
                "ui:placeholder": "Social Media name. eg: Github, LinkedIn",
                "ui:options": {
                    label: false
                }
            },
            url: {
                "ui:placeholder": "Social Media Profile Link",
                "ui:options": {
                    label: false
                }
            }
        }
    }
}

export const personalDetailsData = {
    pd_name: "",
    pd_label: "",
    pd_email: "",
    pd_phone: "",
    pd_website: "",
    pd_address: "",
    pd_profiles: [
        {
            network: "",
            url: "",
            displayUrl: false
        }
    ]
}

export const workExperienceSchema = {
    type: "object",
    title: "Work Experience",
    properties: {
        wk_title: {
            type: "string",
            title: "Work Experience Title"
        },
        wk_workExperience: {
            type: "array",
            title: "Single Work Experience",
            minItems: 1,
            items: {
                "$ref": "#/definitions/experience"
            }
        }
    },
    definitions: {
        experience: {
            type: "object",
            properties: {
                company: {
                    type: "string"
                },
                position: {
                    type: "string"
                },
                from: {
                    type: "string"
                },
                to: {
                    type: "string"
                },
                summary: {
                    type: "string"
                },
                highlights: {
                    type: "array",
                    title: "Highlights",
                    description: "Enter key achievements / highlights and Work related details. Add more lines if required",
                    items: {
                        type: "string"
                    }
                }
            }
        }
    }
}

export const workExperienceUiSchema = {
    classNames: "",
    wk_title: {
        "ui:placeholder": "Title for Work Experience",
        "ui:options": {
            label: false
        }
    },
    wk_workExperience: {
        items: {
            company: {
                "ui:placeholder": "Company Name",
                "ui:options": {
                    label: false
                }
            },
            position: {
                "ui:placeholder": "Position held in Company",
                "ui:options": {
                    label: false
                }
            },
            from: {
                "ui:placeholder": "From: Started working from date",
                "ui:options": {
                    label: false
                }
            },
            to: {
                "ui:placeholder": "To: Worked until date",
                "ui:options": {
                    label: false
                }
            },
            summary: {
                "ui:placeholder": "A short summary of your work",
                "ui:widget": "textarea",
                "ui:options": {
                    label: false
                }
            },
            highlights: {
                items: {
                    "ui:widget": "textarea",
                    "ui:placeholder": "Enter details...",
                }
            }
        }
    }
}

export const workExperienceData = {
    wk_title: "",
    wk_workExperience: [
        {
            company: "",
            position: "",
            from: "",
            to: "",
            summary: "",
            highlights: [
                "",
                ""
            ]
        }
    ]
}

export const educationSchema = {
    type: "object",
    title: "Educational Qualification",
    properties: {
        ed_title: {
            type: "string",
            title: "Education Title"
        },
        ed_educationDetails: {
            type: "array",
            title: "Single Education Detail",
            minItems: 1,
            items: {
                "$ref": "#/definitions/education"
            }
        }
    },
    definitions: {
        education: {
            type: "object",
            properties: {
                institution: {
                    type: "string"
                },
                area: {
                    type: "string"
                },
                studyType: {
                    type: "string"
                },
                from: {
                    type: "string"
                },
                to: {
                    type: "string"
                },
                summary: {
                    type: "string"
                }
            }
        }
    }
}

export const educationUiSchema = {
    classNames: "",
    ed_title: {
        "ui:placeholder": "Title for Education Details",
        "ui:options": {
            label: false
        }
    },
    ed_educationDetails: {
        items: {
            institution: {
                "ui:placeholder": "Institution Name",
                "ui:options": {
                    label: false
                }
            },
            area: {
                "ui:placeholder": "Area of specialization",
                "ui:options": {
                    label: false
                }
            },
            studyType: {
                "ui:placeholder": "Diploma, Degree, Bachelors..",
                "ui:options": {
                    label: false
                }
            },
            from: {
                "ui:placeholder": "From: Course Start date",
                "ui:options": {
                    label: false
                }
            },
            to: {
                "ui:placeholder": "To: Course end date",
                "ui:options": {
                    label: false
                }
            },
            summary: {
                "ui:placeholder": "A short summary of your work",
                "ui:widget": "textarea",
                "ui:options": {
                    label: false
                }
            }
        }
    }
}

export const educationData = {
    ed_title: "",
    ed_educationDetails: [
        {
            institution: "",
            area: "",
            studyType: "",
            from: "",
            to: "",
            summary: ""
        }
    ]
}